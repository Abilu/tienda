﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace Tienda.Models
{
    class Usuarios
    {
        public uint Id { get; set; }
        public string Nombre { get; set; }
        public string Contrasenia { get; set; }

        public override string ToString()
        {
            return $"{Id}) Nombre: {Nombre} Contraseña: {Contrasenia}";
        }

    }
}
